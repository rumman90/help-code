-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Feb 16, 2020 at 04:56 PM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.2.26

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `vue_ecom`
--

-- --------------------------------------------------------

--
-- Table structure for table `mytable`
--

CREATE TABLE `mytable` (
  `id` int(11) NOT NULL,
  `title` varchar(13) NOT NULL,
  `photo` varchar(47) NOT NULL,
  `price` decimal(4,2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `mytable`
--

INSERT INTO `mytable` (`id`, `title`, `photo`, `price`) VALUES
(1, 'Mercedes-Benz', 'http://dummyimage.com/250x250.png/dddddd/000000', '1.31'),
(2, 'Buick', 'http://dummyimage.com/250x250.png/dddddd/000000', '1.39'),
(3, 'Dodge', 'http://dummyimage.com/250x250.png/5fa2dd/ffffff', '8.83'),
(4, 'Audi', 'http://dummyimage.com/250x250.png/dddddd/000000', '3.49'),
(5, 'Mazda', 'http://dummyimage.com/250x250.png/5fa2dd/ffffff', '4.81'),
(6, 'Chrysler', 'http://dummyimage.com/250x250.png/5fa2dd/ffffff', '4.54'),
(7, 'Subaru', 'http://dummyimage.com/250x250.png/cc0000/ffffff', '4.42'),
(8, 'Mercury', 'http://dummyimage.com/250x250.png/ff4444/ffffff', '4.31'),
(9, 'Mercedes-Benz', 'http://dummyimage.com/250x250.png/5fa2dd/ffffff', '4.16'),
(10, 'Oldsmobile', 'http://dummyimage.com/250x250.png/5fa2dd/ffffff', '3.42'),
(11, 'Ford', 'http://dummyimage.com/250x250.png/cc0000/ffffff', '9.92'),
(12, 'Lexus', 'http://dummyimage.com/250x250.png/dddddd/000000', '0.70'),
(13, 'Geo', 'http://dummyimage.com/250x250.png/ff4444/ffffff', '4.38'),
(14, 'Mercedes-Benz', 'http://dummyimage.com/250x250.png/dddddd/000000', '8.22'),
(15, 'Toyota', 'http://dummyimage.com/250x250.png/dddddd/000000', '7.51'),
(16, 'Hyundai', 'http://dummyimage.com/250x250.png/dddddd/000000', '8.65'),
(17, 'Suzuki', 'http://dummyimage.com/250x250.png/ff4444/ffffff', '6.53'),
(18, 'GMC', 'http://dummyimage.com/250x250.png/ff4444/ffffff', '1.91'),
(19, 'Saab', 'http://dummyimage.com/250x250.png/cc0000/ffffff', '0.97'),
(20, 'Porsche', 'http://dummyimage.com/250x250.png/5fa2dd/ffffff', '5.69'),
(21, 'Lamborghini', 'http://dummyimage.com/250x250.png/cc0000/ffffff', '5.21'),
(22, 'Nissan', 'http://dummyimage.com/250x250.png/dddddd/000000', '2.13'),
(23, 'Cadillac', 'http://dummyimage.com/250x250.png/5fa2dd/ffffff', '0.79'),
(24, 'Toyota', 'http://dummyimage.com/250x250.png/cc0000/ffffff', '9.26'),
(25, 'Toyota', 'http://dummyimage.com/250x250.png/ff4444/ffffff', '4.52'),
(26, 'Isuzu', 'http://dummyimage.com/250x250.png/cc0000/ffffff', '9.23'),
(27, 'Hummer', 'http://dummyimage.com/250x250.png/dddddd/000000', '0.32'),
(28, 'Pontiac', 'http://dummyimage.com/250x250.png/5fa2dd/ffffff', '6.42'),
(29, 'Audi', 'http://dummyimage.com/250x250.png/5fa2dd/ffffff', '1.64'),
(30, 'Isuzu', 'http://dummyimage.com/250x250.png/5fa2dd/ffffff', '1.77'),
(31, 'Lexus', 'http://dummyimage.com/250x250.png/dddddd/000000', '8.71'),
(32, 'Toyota', 'http://dummyimage.com/250x250.png/dddddd/000000', '8.88'),
(33, 'Buick', 'http://dummyimage.com/250x250.png/5fa2dd/ffffff', '1.24'),
(34, 'Dodge', 'http://dummyimage.com/250x250.png/ff4444/ffffff', '2.50'),
(35, 'Audi', 'http://dummyimage.com/250x250.png/cc0000/ffffff', '8.67'),
(36, 'Mercury', 'http://dummyimage.com/250x250.png/5fa2dd/ffffff', '8.79'),
(37, 'Mazda', 'http://dummyimage.com/250x250.png/dddddd/000000', '9.29'),
(38, 'Chrysler', 'http://dummyimage.com/250x250.png/dddddd/000000', '1.36'),
(39, 'Ford', 'http://dummyimage.com/250x250.png/cc0000/ffffff', '1.13'),
(40, 'Mazda', 'http://dummyimage.com/250x250.png/dddddd/000000', '7.28'),
(41, 'Hyundai', 'http://dummyimage.com/250x250.png/dddddd/000000', '8.53'),
(42, 'Ford', 'http://dummyimage.com/250x250.png/dddddd/000000', '3.77'),
(43, 'Mercedes-Benz', 'http://dummyimage.com/250x250.png/ff4444/ffffff', '5.93'),
(44, 'Subaru', 'http://dummyimage.com/250x250.png/dddddd/000000', '8.82'),
(45, 'Chevrolet', 'http://dummyimage.com/250x250.png/ff4444/ffffff', '6.20'),
(46, 'Volvo', 'http://dummyimage.com/250x250.png/5fa2dd/ffffff', '6.47'),
(47, 'Volkswagen', 'http://dummyimage.com/250x250.png/ff4444/ffffff', '6.96'),
(48, 'BMW', 'http://dummyimage.com/250x250.png/dddddd/000000', '9.94'),
(49, 'Dodge', 'http://dummyimage.com/250x250.png/5fa2dd/ffffff', '7.77'),
(50, 'GMC', 'http://dummyimage.com/250x250.png/dddddd/000000', '2.54'),
(51, 'Audi', 'http://dummyimage.com/250x250.png/cc0000/ffffff', '7.04'),
(52, 'Aston Martin', 'http://dummyimage.com/250x250.png/5fa2dd/ffffff', '5.69'),
(53, 'GMC', 'http://dummyimage.com/250x250.png/dddddd/000000', '3.74'),
(54, 'Suzuki', 'http://dummyimage.com/250x250.png/5fa2dd/ffffff', '7.76'),
(55, 'Chevrolet', 'http://dummyimage.com/250x250.png/dddddd/000000', '0.62'),
(56, 'Chrysler', 'http://dummyimage.com/250x250.png/ff4444/ffffff', '8.47'),
(57, 'Audi', 'http://dummyimage.com/250x250.png/dddddd/000000', '6.49'),
(58, 'Mercedes-Benz', 'http://dummyimage.com/250x250.png/cc0000/ffffff', '5.12'),
(59, 'Nissan', 'http://dummyimage.com/250x250.png/dddddd/000000', '5.05'),
(60, 'Nissan', 'http://dummyimage.com/250x250.png/ff4444/ffffff', '7.05'),
(61, 'Volkswagen', 'http://dummyimage.com/250x250.png/5fa2dd/ffffff', '8.44'),
(62, 'Saturn', 'http://dummyimage.com/250x250.png/5fa2dd/ffffff', '0.05'),
(63, 'GMC', 'http://dummyimage.com/250x250.png/dddddd/000000', '8.47'),
(64, 'Mazda', 'http://dummyimage.com/250x250.png/dddddd/000000', '8.12'),
(65, 'Porsche', 'http://dummyimage.com/250x250.png/cc0000/ffffff', '9.13'),
(66, 'Chevrolet', 'http://dummyimage.com/250x250.png/ff4444/ffffff', '6.31'),
(67, 'Mitsubishi', 'http://dummyimage.com/250x250.png/ff4444/ffffff', '8.91'),
(68, 'Volkswagen', 'http://dummyimage.com/250x250.png/cc0000/ffffff', '7.02'),
(69, 'Mercedes-Benz', 'http://dummyimage.com/250x250.png/dddddd/000000', '0.24'),
(70, 'Ford', 'http://dummyimage.com/250x250.png/cc0000/ffffff', '5.15'),
(71, 'Toyota', 'http://dummyimage.com/250x250.png/5fa2dd/ffffff', '9.56'),
(72, 'Chrysler', 'http://dummyimage.com/250x250.png/dddddd/000000', '4.26'),
(73, 'Mitsubishi', 'http://dummyimage.com/250x250.png/dddddd/000000', '7.39'),
(74, 'Ford', 'http://dummyimage.com/250x250.png/ff4444/ffffff', '8.60'),
(75, 'Ford', 'http://dummyimage.com/250x250.png/ff4444/ffffff', '4.31'),
(76, 'Suzuki', 'http://dummyimage.com/250x250.png/cc0000/ffffff', '8.53'),
(77, 'Mitsubishi', 'http://dummyimage.com/250x250.png/ff4444/ffffff', '5.23'),
(78, 'Pontiac', 'http://dummyimage.com/250x250.png/5fa2dd/ffffff', '4.42'),
(79, 'Jaguar', 'http://dummyimage.com/250x250.png/cc0000/ffffff', '8.30'),
(80, 'Dodge', 'http://dummyimage.com/250x250.png/dddddd/000000', '5.68'),
(81, 'Nissan', 'http://dummyimage.com/250x250.png/cc0000/ffffff', '5.48'),
(82, 'Volkswagen', 'http://dummyimage.com/250x250.png/ff4444/ffffff', '5.54'),
(83, 'Lincoln', 'http://dummyimage.com/250x250.png/cc0000/ffffff', '8.82'),
(84, 'Hyundai', 'http://dummyimage.com/250x250.png/dddddd/000000', '9.36'),
(85, 'Mitsubishi', 'http://dummyimage.com/250x250.png/5fa2dd/ffffff', '1.76'),
(86, 'Honda', 'http://dummyimage.com/250x250.png/dddddd/000000', '8.73'),
(87, 'Lincoln', 'http://dummyimage.com/250x250.png/5fa2dd/ffffff', '3.96'),
(88, 'Dodge', 'http://dummyimage.com/250x250.png/dddddd/000000', '4.88'),
(89, 'Pontiac', 'http://dummyimage.com/250x250.png/dddddd/000000', '5.89'),
(90, 'Mercedes-Benz', 'http://dummyimage.com/250x250.png/cc0000/ffffff', '7.20'),
(91, 'Mercedes-Benz', 'http://dummyimage.com/250x250.png/dddddd/000000', '3.93'),
(92, 'Audi', 'http://dummyimage.com/250x250.png/cc0000/ffffff', '0.70'),
(93, 'Saturn', 'http://dummyimage.com/250x250.png/5fa2dd/ffffff', '5.47'),
(94, 'Chevrolet', 'http://dummyimage.com/250x250.png/dddddd/000000', '5.83'),
(95, 'Lexus', 'http://dummyimage.com/250x250.png/ff4444/ffffff', '9.86'),
(96, 'Jeep', 'http://dummyimage.com/250x250.png/cc0000/ffffff', '3.68'),
(97, 'BMW', 'http://dummyimage.com/250x250.png/5fa2dd/ffffff', '7.32'),
(98, 'Lincoln', 'http://dummyimage.com/250x250.png/cc0000/ffffff', '9.72'),
(99, 'Toyota', 'http://dummyimage.com/250x250.png/dddddd/000000', '4.70'),
(100, 'Ford', 'http://dummyimage.com/250x250.png/ff4444/ffffff', '5.84');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `mytable`
--
ALTER TABLE `mytable`
  ADD PRIMARY KEY (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
